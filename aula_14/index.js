const express = require('express')
const app = express()
app.use(express.json());
const port = 3000

const CarroAutomatico = require("./CarroAutomatico")
var carros = [];

app.get('/carros', (req, res) => {
  res.json({carros:carros.map(carro => carro.toJson())});
});

app.post('/carro', (req, res) => {
  let carroBody = req.body;
  console.log(carroBody);

  carro = new CarroAutomatico();
  carro.setNome(carroBody.nome);
  carro.setMarca(carroBody.marca);
  carro.setCor(carroBody.cor);

  carros.push(carro);

  res.json({
    sucesso: true,
    data: carro.toJson()
  });
})


app.delete('/carro', (req, res) => {
  let carroBody = req.body;
  let nome = carroBody.nome;
  carros = carros.filter(carro => carro.nome != nome);
  res.json({carros:carros.map(carro => carro.toJson())});
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})