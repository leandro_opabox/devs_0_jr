const Veiculo = require("./Veiculo")

class CarroAutomatico extends Veiculo {
    constructor() {
        super();
        this.cambio = "automatico";
    }

    acelerar () {
        return true;
    }

    setCor(cor) {
        this.cor = cor;
    }

    setMarca(marca) {
        this.marca = marca;
    }

    setNome(nome) {
        this.nome = nome;
    }

    toJson() {
        return {
            "nome": this.nome,
            "marca": this.marca,
            "cambio": this.cambio,
            "rodas": this.rodas
        }
    }
    
}

module.exports = CarroAutomatico;