const Service = require("./Service");
const FileRepo = require("../repository/FileRepo");
const CarroAutomatico = require("../models/CarroAutomatico");

class CarroService extends Service{
    constructor() {
        super()
        this.file = new FileRepo();
    }

    async getCarro(nome) {
        try {
            let carrosFile = await this.file.getFile(this.CARRO_FILE_NAME);
            let carrosFiltrados = carrosFile.filter(carro => carro.nome === nome);
            if (carrosFiltrados.length > 0) {
                return carrosFiltrados[0];
            } else {
                return null;
            }
        } catch (e) {
            console.log(e);
            throw e;
        }
    }

    async postCarro(carroBody) {
        let carro = new CarroAutomatico();
        carro.setNome(carroBody.nome);
        carro.setMarca(carroBody.marca);
        carro.setCor(carroBody.cor);

        try {
            let carrosFile = await this.file.getFile(this.CARRO_FILE_NAME);
            carrosFile.push(carro.toJson());
            await this.file.saveFile(this.CARRO_FILE_NAME, carrosFile)
            return carro.toJson();
        }catch(e) {
            console.log(e);
            throw e;
        }
    }

    async deleteCarro(nome) {
        try {
            let carrosFile = await this.file.getFile(this.CARRO_FILE_NAME);
            let carrosFiltrados = carrosFile.filter(carro => carro.nome === nome);
            await this.file.saveFile(this.CARRO_FILE_NAME, carrosFiltrados)
            return carrosFiltrados;
        } catch (e) {
            console.log(e);
            throw e;
        }
    }
}

module.exports = CarroService;