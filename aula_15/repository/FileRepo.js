const fs = require("fs");
const fsPromises = fs.promises;

class FileRepo {

    constructor () {
        this.filePath = __dirname + "/../files";
    }

    async saveFile(name, data) {
        try {
            await fsPromises.writeFile( `${this.filePath}/${name}`, JSON.stringify(data));
            return true;
        } catch(e) {
            console.log(e);
            throw e;
        }
    }

    async getFile(name) {
        try {
            let data =  await fsPromises.readFile( `${this.filePath}/${name}`);
            if (!data.length) {
                data = [];
            } else {
                data = JSON.parse(data);
            }
            return data;
        } catch(e) {
            console.log(e);
            if (e.code === 'ENOENT') {
                return [];
            }
        }
    } 
    
}

module.exports = FileRepo;