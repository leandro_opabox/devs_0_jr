var express = require('express');
var router = express.Router();
const CarroService = require("../service/CarroService");
const carroService = new CarroService();

router.get('/:nome', async (req, res) => {
    try {
        let nome = req.params.nome;
        let carro = await carroService.getCarro(nome)
        res.json({
            error: false,
            data: {carro:carro}
        });
    } catch(e) {
        console.log(e);
        res.json({ error: true, exception: e});
    }
})


router.post('/', async (req, res) => {
    try {
        let carroBody = req.body;
        console.log(carroBody);
        let retorno = await carroService.postCarro(carroBody);
        res.json({
            error: false,
            data: retorno
        });
    } catch(e) {
        console.log(e);
        res.json({ error: true, exception: e});
    }
});
  
  
router.delete('/', async (req, res) => {
    
})

module.exports = router;