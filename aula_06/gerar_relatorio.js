const fs = require('fs');

console.log("iniciar");
console.log("ler arquivo");


var arquivo;
try {
     arquivo = fs.readFileSync('./arquivos/massa.csv', "utf8");
} catch(e) {
    console.log(e);
}

if (arquivo != null) {

    console.log("armazenar o conteudo do arquivo linha a linha em registros");
    let registros = arquivo.split("\n");

    //declaracao das variaveis
    var NR_TOTAL = 0;
    var PAGINA = 1;
    var PAGINA_TOTAL = 0;
    var PAGINAS = [];
    var REGISTRO = 0;
    var LISTA_CLIENTE = [];

    // inicio do looping de registros armazenados na variavel;
    for ( NR_TOTAL=0; NR_TOTAL < registros.length; NR_TOTAL++ ) {

        if (REGISTRO === 10) {
            console.log('nova pagina');
            PAGINAS.push({
                numero: PAGINA,
                clientes: LISTA_CLIENTE,
                total: PAGINA_TOTAL
            });
            REGISTRO = 0;
            LISTA_CLIENTE = [];
            PAGINA = PAGINA + 1;
            //PAGINA_TOTAL = 0;
        } 

        var saldo = Number(registros[NR_TOTAL].split(";")[2]);
        if (saldo >= 1500) {
            REGISTRO = REGISTRO + 1;
            PAGINA_TOTAL = PAGINA_TOTAL + saldo;
            LISTA_CLIENTE.push(registros[NR_TOTAL]);
            console.log(`cliente adicionado ${saldo}`);
        } else {
            console.log( `ignora cliente saldo menor ${saldo}`);
        }

    }
    // final do laço NR_TOTAL não é menor que o tamanho dos registros
    // escre pagina final com o residuo da divisao por 10
    console.log('nova pagina');
    PAGINAS.push({
        numero: PAGINA,
        clientes: LISTA_CLIENTE,
        total: PAGINA_TOTAL
    });


    // escreve o relatorio com as paginas
    console.log("Escrever Relatório");
    var relatorio = "";

    for ( var x =0; x<PAGINAS.length; x++ ){

        relatorio = relatorio + `-------------------\nPAGINA ${PAGINAS[x].numero}\n-------------------\n\n`;
        var clientes = PAGINAS[x].clientes;

        for (var y =0; y<clientes.length; y++){
            relatorio = relatorio + clientes[y].replace(/;/g, " | ") + "\n";
        }

        relatorio = relatorio + `-------------------\nTOTAL DA PAGINA ${PAGINAS[x].total}\n-------------------\n\n\n`;

    }

    fs.writeFile("./arquivos/relatorio.txt", relatorio, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("relatorio salvo!");
    }); 

} else {
    console.log("erro ao ler arquivo de dados");
}