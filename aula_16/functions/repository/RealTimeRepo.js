const admin = require("firebase-admin");
admin.initializeApp();

class RealTimeTepo {

    constructor() {
        
    }

    getCarro(nome) {
        return new Promise((resolve, reject) => {
            let result;
            admin.database().ref("/carros")
            .once("value", (carros) => {
                result = carros.val();
                let keys = Object.keys(result);
                let carro = null;
                for (let x=0; x<keys.length; x++) {
                    if (result[keys[x]].nome === nome) {
                        carro = result[keys[x]];
                        break;
                    }
                }
                resolve(carro);
            });
        })
    }

    async postCarro(carro) {
        try { 
            let result = await admin.database().ref('/carros').push(carro);
            return result;
        } catch(e) {
            console.log(e);
            throw e;
        }
    }

}

module.exports = RealTimeTepo;