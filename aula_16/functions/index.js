const functions = require("firebase-functions");
const express = require('express');
const router = require("./routes/router");
const app = express();

app.use(express.json());
app.use("/", router);

exports.apiv1 = functions.https.onRequest(app);