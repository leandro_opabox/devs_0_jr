var express = require('express');
var router = express.Router();


// the cors filter
const cors = require("./cors");
router.use(cors);

const carroRoutes = require("./CarroRoutes.js");
router.use("/carro", carroRoutes);

const carrosRoutes = require("./CarrosRoutes");
router.use("/carros", carrosRoutes);

module.exports = router;