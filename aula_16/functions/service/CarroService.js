const Service = require("./Service");
//const FileRepo = require("../repository/FileRepo");
const RealTimeRepo = require("../repository/RealTimeRepo");
const CarroAutomatico = require("../models/CarroAutomatico");

class CarroService extends Service{
    constructor() {
        super()
        //this.file = new FileRepo();
        this.repo = new RealTimeRepo();
    }

    async getCarro(nome) {
        try {
            let carro = await this.repo.getCarro(nome);
            return carro;
        } catch(e) {
            console.log(e);
            throw e;
        }
    }

    async postCarro(carroBody) {

        let carro = new CarroAutomatico();
        carro.setNome(carroBody.nome);
        carro.setMarca(carroBody.marca);
        carro.setCor(carroBody.cor);
        try {
            return await this.repo.postCarro(carro.toJson());
        } catch(e) {
            console.log(e);
            throw e;
        }
    }

    async deleteCarro(nome) {
        
    }
}

module.exports = CarroService;