
// decalaracao de constantes globais
const LARGURA_GAME = 320;
const ALTURA_GAME = 320;
const LARGURA_TILE = 100;
const ALTURA_TILE = 100;
const LARGURA_DIVISORIA = 10;
const JOGADOR_X = "X";
const JOGADOR_Y = "O";

// declaracao de variaveis globais
var canvas;
var ctx;
var matrizJogo = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0],
];
var vezDoJogador = JOGADOR_X;

function setup () {

    console.log("INICIA O SETUP");

    canvas = document.getElementById("canvas");
    ctx = canvas.getContext("2d");

    //captura o clique
    canvas.addEventListener("mousedown", capturaClique);
    
    setInterval(loop,1000/30);  

}

function loop() {
    desenha();
}

function desenha () {
    desenhaTelaDoJogo();
    desenhaJogadasNoGrid();
}

function adicionaJogadaNaMatriz (quadranteX, quadranteY) {

    let jogada = obtemJogada();
    
    if (!verificaFinalDoJogo()) {
        if (matrizJogo[quadranteX][quadranteY] === 0) {
            matrizJogo[quadranteX][quadranteY] = jogada;
        }
    } else {
        limpaJogo();
    }
    
}

function obtemJogada () {
    let jogada = vezDoJogador;

    if (vezDoJogador === JOGADOR_X) {
        vezDoJogador = JOGADOR_Y;
    } else {
        vezDoJogador = JOGADOR_X;
    }

    return jogada;
}

function verificaFinalDoJogo () {

    let acabou = true;

    // verifica se acabou com grid completo
    for (let x=0; x < matrizJogo.length; x++) {
        for (let y=0; y <  matrizJogo[x].length; y++) {
            if (matrizJogo[x][y] === 0) {
                acabou = false;
                break;
            }
        }
    }

    //verifica se acabou com linhas horizontais completas
    // e iguais
    for (let x=0; x < 3; x++) {
        if (matrizJogo[x][0] !== 0 && 
            matrizJogo[x][0] === matrizJogo[x][1] &&
            matrizJogo[x][0] === matrizJogo[x][2] ) {
                console.log("ACABOU vencedor vertical");
                acabou = true;
                break;
            }
    }

    //verifica se acabou com linhas verticais completas
    // e iguais
    for (let y=0; y < 3; y++) {
        if (matrizJogo[0][y] !== 0 && 
            matrizJogo[0][y] === matrizJogo[1][y] &&
            matrizJogo[0][y] === matrizJogo[2][y] ) {
                console.log("ACABOU vencedor horizontal");
                acabou = true;
                break;
            }
    }

    //verifica se o jogo acabou com vencedor nas diagonais
    if (matrizJogo[0][0] !== 0 && 
        matrizJogo[0][0] === matrizJogo[1][1] &&
        matrizJogo[0][0] === matrizJogo[2][2] ) {
            console.log("ACABOU diagonal 1");
            acabou = true;
    }
    if (matrizJogo[2][0] !== 0 && 
        matrizJogo[2][0] === matrizJogo[1][1] &&
        matrizJogo[2][0] === matrizJogo[0][2] ) {
            console.log("ACABOU diagonal 2");
            acabou = true;
    }

    return acabou;
}

function limpaJogo() {
    matrizJogo = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ];
}

function desenhaJogadasNoGrid() {
    for (let x=0; x < matrizJogo.length; x++) {
        for (let y=0; y <  matrizJogo[x].length; y++) {
            if (matrizJogo[x][y] !== 0) {
                desenhaXouONoTile(matrizJogo[x][y], x, y);
            }
        }
    }
}

// FUNCOES UTILITARIAS

function desenhaTelaDoJogo () {
    //desenha grid preto
    ctx.fillStyle = "#000000";
    ctx.fillRect(0,0,LARGURA_GAME, ALTURA_GAME);
    //desenha divisorias verticais
    ctx.fillStyle = "#ffffff";
    ctx.fillRect(LARGURA_TILE,0, LARGURA_DIVISORIA, ALTURA_GAME);
    ctx.fillRect(LARGURA_TILE*2+LARGURA_DIVISORIA,0, LARGURA_DIVISORIA, ALTURA_GAME);
    //desenha divisorias horizontais
    ctx.fillRect(0, ALTURA_TILE, LARGURA_GAME, LARGURA_DIVISORIA);
    ctx.fillRect(0, ALTURA_TILE*2+LARGURA_DIVISORIA, LARGURA_GAME, LARGURA_DIVISORIA);
    //resesta a cor para preto
    ctx.fillStyle = "#000000";
}

function desenhaXouONoTile(jogada, tileX, tileY) {
    ctx.fillStyle = "#ffffff";
    ctx.font = "60px monospace";
    margemX = tileX * LARGURA_DIVISORIA;
    margemY = tileY * LARGURA_DIVISORIA;
    ctx.fillText(jogada, LARGURA_TILE * tileX + (LARGURA_TILE/3) + margemX, ALTURA_TILE * tileY + (ALTURA_TILE / 1.5) + margemY);
    ctx.fillStyle = "#000000";
}

// retorna o clique do mouse ja convertido em quadrante
function capturaClique(evento) {
    let mouseX = evento.offsetX;
    let mouseY = evento.offsetY;

    let tileX = Math.floor(mouseX / (LARGURA_TILE + ((2*LARGURA_DIVISORIA)/3)));
    let tileY = Math.floor(mouseY / (ALTURA_TILE + ((2*LARGURA_DIVISORIA)/3)));

    console.log(`quadrante ${tileX} - ${tileY}`);

    adicionaJogadaNaMatriz(tileX, tileY);
}

// INICIALIZA O GAME
setup();