
// decalaracao de constantes globais
const LARGURA_GAME = 320;
const ALTURA_GAME = 320;
const LARGURA_TILE = 100;
const ALTURA_TILE = 100;
const LARGURA_DIVISORIA = 10;
const JOGADOR_X = "X";
const JOGADOR_Y = "O";

// declaracao de variaveis globais
var canvas;
var ctx;
var matrizJogo = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0],
];
var vezDoJogador = JOGADOR_X;

function setup () {

    console.log("INICIA O SETUP");

    canvas = document.getElementById("canvas");
    ctx = canvas.getContext("2d");

    //captura o clique
    canvas.addEventListener("mousedown", capturaClique);
    
    setInterval(loop,1000/30);  

}

function loop() {
   // adicione a lógica
}

function desenha () {
    // adicione a lógica
}

function adicionaJogadaNaMatriz (quadranteX, quadranteY) {
    // adicione a lógica
}

function obtemJogada () {
    // adicione a lógica
}

function verificaFinalDoJogo () {
    // adicione a lógica
}

function limpaJogo() {
    // adicione a lógica
}

function desenhaJogadasNoGrid() {
    // adicione a lógica
}

// FUNCOES UTILITARIAS

function desenhaTelaDoJogo () {
    //desenha grid preto
    ctx.fillStyle = "#000000";
    ctx.fillRect(0,0,LARGURA_GAME, ALTURA_GAME);
    //desenha divisorias verticais
    ctx.fillStyle = "#ffffff";
    ctx.fillRect(LARGURA_TILE,0, LARGURA_DIVISORIA, ALTURA_GAME);
    ctx.fillRect(LARGURA_TILE*2+LARGURA_DIVISORIA,0, LARGURA_DIVISORIA, ALTURA_GAME);
    //desenha divisorias horizontais
    ctx.fillRect(0, ALTURA_TILE, LARGURA_GAME, LARGURA_DIVISORIA);
    ctx.fillRect(0, ALTURA_TILE*2+LARGURA_DIVISORIA, LARGURA_GAME, LARGURA_DIVISORIA);
    //resesta a cor para preto
    ctx.fillStyle = "#000000";
}

function desenhaXouONoTile(jogada, tileX, tileY) {
    ctx.fillStyle = "#ffffff";
    ctx.font = "60px monospace";
    margemX = tileX * LARGURA_DIVISORIA;
    margemY = tileY * LARGURA_DIVISORIA;
    ctx.fillText(jogada, LARGURA_TILE * tileX + (LARGURA_TILE/3) + margemX, ALTURA_TILE * tileY + (ALTURA_TILE / 1.5) + margemY);
    ctx.fillStyle = "#000000";
}

// retorna o clique do mouse ja convertido em quadrante
function capturaClique(evento) {
    let mouseX = evento.offsetX;
    let mouseY = evento.offsetY;

    let tileX = Math.floor(mouseX / (LARGURA_TILE + ((2*LARGURA_DIVISORIA)/3)));
    let tileY = Math.floor(mouseY / (ALTURA_TILE + ((2*LARGURA_DIVISORIA)/3)));

    console.log(`quadrante ${tileX} - ${tileY}`);

    adicionaJogadaNaMatriz(tileX, tileY);
}

// INICIALIZA O GAME
setup();