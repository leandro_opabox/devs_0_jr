const express = require('express')
const app = express()
app.use(express.json());
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/user', (req, res) => {
  let user = req.body
  console.log(req)
  res.send(user)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})