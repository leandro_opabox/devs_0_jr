
const fs = require('fs');

// ----- CALBACK ----------

 console.log("1");

// function chamarMaisUmaVez(erro, dadosSucesso) {
//     if (!erro) {
//         console.log(String(dadosSucesso));
//     } else {
//         console.log(erro);
//     }
// }
// function chamarDeVolta(erro, dadosSucesso) {
//     if (!erro) {
//         console.log(String(dadosSucesso));
//         fs.readFile("./numeros2.txt", chamarMaisUmaVez);
//     } else {
//         console.log(erro);
//    }
// }
// fs.readFile("./numeros.txt", chamarDeVolta);

 console.log(2);
 console.log(3);

// ----------- CALLBACK HELL

// console.log(1);


// fs.readFile("./numeros.txt", (erro, dados) => {
//     fs.readFile("./numeros2.txt", (erro2, dados2) => {
//         fs.readFile("./numeros3.txt", (erro3, dados3) => {
//             console.log(String(dados));
//             console.log(String(dados2));
//             console.log(String(dados3));
//         });
//     });
// });

// console.log(2);

// console.log(3);


// -----------     PROMISE -------------

// console.log("1");

// const promessa = new Promise((resolve, reject) => {
//     fs.readFile("./numeros.txt", (erro, data) => {
//         if (!erro) {
//             resolve(data);
//         } else {
//             reject(erro);
//         }
//     })
// }).then((resultado) => {
//     console.log(String(resultado));
// }).catch((erro) => {
//     console.log(erro);
// })

// console.log(2);
// console.log(3);

// promessa 2

// console.log(1);

// const promessa = new Promise((resolve, reject) => {
//     fs.readFile("./numeros.txt", (erro, data) => {
//         if (!erro) {
//             resolve(data);
//         } else {
//             reject(erro);
//         }
//     })
// });

// console.log(promessa);

// console.log("2");

// console.log("3");

// promessa 3

// console.log(1);

// const promessa = new Promise((resolve, reject) => {
//     fs.readFile("./numeros.txt", (erro, data) => {
//         if (!erro) {
//             resolve(data);
//         } else {
//             reject(erro);
//         }
//     })
// });

// const promessa2 = new Promise((resolve, reject) => {
//     fs.readFile("./numeros2.txt", (erro, data) => {
//         if (!erro) {
//             resolve(data);
//         } else {
//             reject(erro);
//         }
//     })
// });

// var promessas = Promise.all([promessa, promessa2]);
// promessas.then( values => {
//     values.forEach(element => console.log(String(element)));
// });

// console.log("2");

// console.log("3");

// ---- ASYNC AWAIT

console.log(1);

function lerArquivo(caminho) {
    return new Promise((resolve, reject) => {
        try {
            fs.readFile(caminho, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            })
        } catch (e) {
            reject(e);
        }
    });
}


// async function init() {
//     try {
//         const result1 = await lerArquivo("./numeros.txt");
//         const result2 = await lerArquivo("./numeros2.txt");
//         const result3 = await lerArquivo("./numeros3.txt");
//         console.log(String(result1));
//         console.log(String(result2));
//         console.log(String(result3));
//     } catch(e) {
//         console.log(e);
//     }

// }


// init();

console.log("2");

console.log("3");

let promessasListas = [];

for ( let x=0; x< 10; x++) {
    promessasListas.push((async () => {
        const result1 = await lerArquivo("./numeros.txt");
        return result1;
    })());
}

var promessas = Promise.all(promessasListas);
promessas.then( values => {
    values.forEach(element => console.log(String(element)));
});

console.log(4);
// ------------------